import { library } from '@fortawesome/fontawesome-svg-core';
import { faBitbucket, faGithub, faPatreon, faTwitch, faTwitter } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import Vue from 'vue';
import VueAnalytics from 'vue-analytics';
import LoadScript from 'vue-plugin-load-script';

import App from './App.vue';
import router from './router';
import store from './store';

library.add(faTwitch, faGithub, faBitbucket, faTwitter, faPatreon)

Vue.component('icon', FontAwesomeIcon)

Vue.use(VueAnalytics, {
	id: ['UA-105690615-1']
})
Vue.use(LoadScript)

Vue.config.productionTip = false

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app')