import axios from 'axios';

function Api() {
	return axios.create({
		baseURL: "https://api.twitch.tv/kraken/streams/",
		headers: {
			"Client-ID": "ib0c5vqwerveutvngp3csxzjyw9p2r"
		}
	});
}

export default {
	getStreamStatus(stream) {
		return Api().get(stream);
	}
}